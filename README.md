These are the init.d scripts for Marla/Moretz.
==================

A brief guide on how to include this in your own CM10 ROM.
------------------


Add the following to your local_manifest.xml:

	<remote  name="bitbucket"
		fetch="ssh://git@bitbucket.org" />
  	<project name="carvsdriver/vendor_moretz-kernel" 
		path="vendor/moretz-kernel"
		remote="bitbucket"
		revision="master" /> 

Next, you'll need to update your device/samsung/skyrocket/cm.mk file with this:

	$(call inherit-product, vendor/moretz-kernel/moretz-kernel.mk)

Change skyrocket in the above to your device as necessary (hercules, exhilarate).

~CvD
